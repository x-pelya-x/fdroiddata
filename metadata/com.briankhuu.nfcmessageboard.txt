Categories:Office
License:GPLv3+
Web Site:
Source Code:https://github.com/mofosyne/NFCMessageBoard
Issue Tracker:https://github.com/mofosyne/NFCMessageBoard/issues

Auto Name:NFCMessageBoard
Summary:Share messages via NFC tags
Description:
Proof of concept for a localized message system.

It is quite simple, and does the job of reading and writing to any plain text
NFC tags. At a minimum you are recommended to use an NTAG216 that gives you
approximately 800 bytes of writeable memory.

It also has the advantage of being relatively private means of communicating
(even if it runs the issue of spoofing), since it is essentially a dead drop
system.

Potential Uses:

* Geo Cache GuestBook
* Ingress Local Message Store

Features:

* Reads Plain Text
* Write and prepends a message to a Plain Text Tag
* UTF-8 support, so can show emoticon.
* Links, email, map address are autolinked
.

Repo Type:git
Repo:https://github.com/mofosyne/NFCMessageBoard

Build:1.0,1
    commit=87630a2348e9e2a6c7bf82fdb541188aa3f71760
    subdir=app
    gradle=yes

Build:1.2,3
    commit=d1254957f3374977ea21fce50d91c38e4db9d47b
    subdir=app
    gradle=yes

Build:1.4,5
    commit=34da04176cf6594aec572b698b9bcff6d67d58f9
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4
Current Version Code:5

