Categories:Internet
License:Apache2
Web Site:https://duck.co/help/mobile/android
Source Code:https://github.com/duckduckgo/android
Issue Tracker:https://github.com/duckduckgo/android/issues

Auto Name:DuckDuckGo
Summary:Search widget
Description:
Search the web via duckduckgo.com, a search engine that's focussed
on privacy.

The app also works like a news reader, showing popular stories from a
customizable list of websites, until you enter a query.

Works with [[org.torproject.android]] (though not on Android 4.4).

Please note that using this app as a browser will expose your visited
sites to DuckDuckGo.
.

Repo Type:git
Repo:https://github.com/duckduckgo/android.git

Build:2.1.1,52
    commit=v2.1.1
    srclibs=1:NetCipher@4fe34ede3f44d968e55

Build:2.1.2,53
    commit=v2.1.2
    submodules=yes

Build:2.1.3,54
    commit=v2.1.3
    submodules=yes

Build:2.1.4,55
    commit=v2.1.4
    submodules=yes

Build:2.1.6,57
    commit=v2.1.6
    submodules=yes
    prebuild=cp libs/android-support-v4.jar libs/OnionKit/libnetcipher/libs/android-support-v4.jar

Build:2.1.9,60
    commit=683ce1862af5aaa59bb4368ceae46312c57b64ef
    submodules=yes
    prebuild=cp libs/android-support-v4.jar libs/OnionKit/libnetcipher/libs/android-support-v4.jar

Build:2.1.10,61
    commit=c60349d8eacc02dc612ccc137d55816f66b8a876
    submodules=yes
    prebuild=cp libs/android-support-v4.jar libs/OnionKit/libnetcipher/libs/android-support-v4.jar

Build:3.0.2,64
    commit=ca829f96f7ade3009cd1c2006c6ce0771704df37
    submodules=yes
    gradle=yes
    rm=libs/picasso*.jar,libs/acra*.jar,libs/otto*.jar
    prebuild=sed -i -e '/fileTree/acompile "com.squareup:otto:1.3.7"\ncompile "ch.acra:acra:4.5.0"\ncompile "com.squareup.picasso:picasso:2.5.2"' build.gradle

Build:3.0.3,65
    commit=v3.0.3
    submodules=yes
    gradle=yes
    rm=libs/picasso*.jar,libs/acra*.jar,libs/otto*.jar
    prebuild=sed -i -e '/fileTree/acompile "com.squareup:otto:1.3.7"\ncompile "ch.acra:acra:4.5.0"\ncompile "com.squareup.picasso:picasso:2.5.2"' build.gradle

Build:3.0.4,66
    commit=v3.0.4
    submodules=yes
    gradle=yes
    rm=libs/picasso*.jar,libs/acra*.jar,libs/otto*.jar
    prebuild=sed -i -e '/fileTree/acompile "com.squareup:otto:1.3.7"\ncompile "ch.acra:acra:4.5.0"\ncompile "com.squareup.picasso:picasso:2.5.2"' build.gradle

Build:3.0.5,67
    commit=bac864818338252361960ea4f0ee18f906e47799
    submodules=yes
    gradle=yes
    rm=libs/picasso*.jar,libs/acra*.jar,libs/otto*.jar
    prebuild=sed -i -e '/fileTree/acompile "com.squareup:otto:1.3.7"\ncompile "ch.acra:acra:4.5.0"\ncompile "com.squareup.picasso:picasso:2.5.2"' build.gradle

Build:3.0.6,68
    commit=897ad0f4b7996217f4ea4ab819262dbaeb759893
    submodules=yes
    gradle=yes
    rm=libs/picasso*.jar,libs/acra*.jar,libs/otto*.jar
    prebuild=sed -i -e '/fileTree/acompile "com.squareup:otto:1.3.7"\ncompile "ch.acra:acra:4.5.0"\ncompile "com.squareup.picasso:picasso:2.5.2"' build.gradle

Build:3.0.7,69
    commit=16655ecf6951ddae4a4672fd6ce6a3809c28c008
    submodules=yes
    gradle=yes
    rm=libs/picasso*.jar,libs/acra*.jar,libs/otto*.jar
    prebuild=sed -i -e '/fileTree/acompile "com.squareup:otto:1.3.7"\ncompile "ch.acra:acra:4.5.0"\ncompile "com.squareup.picasso:picasso:2.5.2"' build.gradle

Maintainer Notes:
* Reset to UCM:Tags when https://github.com/duckduckgo/android/issues/145 is solved.
  [tags are missing from time to time]
* Removed some jar files (todo: appcompat -> maybe required by netcipher)
* Maybe qualifies for AUM afterwards?
.

Auto Update Mode:None
#Update Check Mode:Tags
Update Check Mode:RepoManifest
Current Version:3.0.7
Current Version Code:69

