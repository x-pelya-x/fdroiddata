Categories:Games
License:GPLv3+
Web Site:http://pixeldungeon.watabou.ru
Source Code:https://github.com/watabou/pixel-dungeon
Issue Tracker:https://github.com/watabou/pixel-dungeon/issues
Changelog:http://pixeldungeon.wikia.com/wiki/Update
Bitcoin:1LyLJAzxCfieivap1yK3iCpGoUmzAnjdyK

Auto Name:Pixel Dungeon
Summary:Rogue-like
Description:
Traditional roguelike game with pixel-art graphics and simple interface.
.

Repo Type:git
Repo:https://github.com/watabou/pixel-dungeon

#force close on emulator, but works on bare metal
Build:1.7.1c,59
    commit=33c27cdb587878f771e999103ba186f190f9446c
    patch=types-171c.patch
    srclibs=PDClasses@4902d172b0fccede3acce833efe9b4d5d77b9c33
    prebuild=cp -R $$PDClasses$$/com/watabou/* src/com/watabou/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-18

Build:1.7.2a,61
    commit=3ce26c2541dcf9d1f01642289acb4495586b50b0
    patch=types-172a.patch
    srclibs=PDClasses@9ee29184de9a569c2435929f4f0d435d4edd6f48
    prebuild=cp -R $$PDClasses$$/com/watabou/* src/com/watabou/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Build:1.7.5a,70
    commit=8760fd8f4a33676aff70f7442c8fc4c4774dfbb6
    patch=types-175a.patch
    srclibs=PDClasses@2a7faeb1cfb2a8143887392eb209e0d102ba35a4
    prebuild=cp -R $$PDClasses$$/com/watabou/* src/com/watabou/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-19

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.7.5a
Current Version Code:70

