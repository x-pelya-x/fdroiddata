Categories:Internet
License:GPLv2+
Web Site:https://github.com/Skarafaz/mercury/wiki
Source Code:https://github.com/Skarafaz/mercury
Issue Tracker:https://github.com/Skarafaz/mercury/issues

Auto Name:Mercury-SSH
Summary:Send preconfigured commands via SSH
Description:
Sends pre-configured commands via SSH to remote servers with just a tap. You
will need to create a config file to setup communication with your server
using [https://github.com/Skarafaz/mercury/wiki the wiki documentation].
.

Repo Type:git
Repo:https://github.com/Skarafaz/mercury

Build:1.0.0,1
    commit=v1.0.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.1
Current Version Code:2

